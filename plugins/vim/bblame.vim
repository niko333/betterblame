""" plug bblame into vim
" This plugin supports terminal vim/neovim

" So far this plugin adds one user command "Bblame" which opens bblame on the
" current file in visual mode on the current line the vim cursor is on

" INSTALL:
" Copy this file to the plugins dir of your vim dir, e.g.: ~/.vim/plugin/

" Map command Bblame to Bblame func
com -nargs=* Bblame call Bblame(<f-args>)

" Command that will shell out to bblame for browsing the history of a file,
" then return back to vim upon exiting.
function! Bblame()  " [!] replace any existing Bblame funcs
    " escape any special chars before calling bblame
    if has("nvim")
        silent execute "term bblame " . shellescape(expand('%')) . " +" . shellescape(line('.'))
    else
        silent execute "!bblame " . shellescape(expand('%')) . " +" . shellescape(line('.'))
    endif
    execute(':redraw!')
endfunction

