src_dir = betterblame
doctest_files = $(shell find betterblame/ \( -name "*.py" ! -name "__*__.py" \))

check: flake8 doctests pylint unittests rstcheck

flake8:
	@echo "### Running - flake8"
	flake8 --statistics $(src_dir)

pylint:
	@echo "### Running - pylint"
	scripts/pylint_runner.sh $(src_dir)

unittests:
	@echo "### Running - unittests"
	PYTHONPATH=. pytest -v

doctests:
	@echo "### Running - doctests"
	PYTHONPATH=. pytest -v --doctest-modules $(doctest_files)

rstcheck:
	@echo "### Running - rstcheck"
	rstcheck README.rst

build: clean
	@echo "### Building"
	python setup.py sdist

upload-test: build
	@echo "### Uploading a test build"
	twine upload --repository testpypi dist/*.tar.gz

upload: build
	@echo "### Uploading a build"
	twine upload dist/*.tar.gz

# bblame_deps installed for both python 2.7.X and python 3+
py_env:
	@echo "### Installing python pip dependencies"
	pip3 install -r env/dev_test_deps.txt
	pip3 install -r env/bblame_deps.txt
	pip install -r env/bblame_deps.txt

clean:
	@echo "### Cleaning dist"
	rm -rf *.egg-info
	rm -rf dist
