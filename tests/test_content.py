"""Tests for the line functions and classes for bblame"""
from betterblame import content
from betterblame.options import UserOptionManager, SHORTEN_FILEPATHS_OPT
import curses


def test_line():
    """Test the Line object"""
    line = content.Line(['one ', 'two ', 'three'])

    # Test standard operations
    assert(line.full_text() == 'one two three')
    assert(len(line) == 3)
    assert(line[1].text == 'two ')

    for segment in line:
        assert(segment.curses_attrs == content.DEFAULT_CURSES_ATTR)

    # Test adding a new segment
    line.add_segment(' four', segment_attributes=curses.A_BOLD)

    assert(len(line) == 4)
    assert(line[-1].text == ' four')
    assert(line[-1].curses_attrs == curses.A_BOLD)

    # Test that we don't support index assignment
    try:
        line[1] = '1.5'
    except NotImplementedError:
        pass

    assert(line.full_text() == 'one two three four')

    # Check width slicing
    sub_line = line.prepare_line_for_printing(start=0, end=8)
    assert(sub_line.full_text() == 'one two ')

    sub_line = line.prepare_line_for_printing(start=0, end=800)
    assert(sub_line.full_text() == 'one two three four')

    sub_line = line.prepare_line_for_printing(start=0, end=0)
    assert(sub_line.full_text() == '')

    sub_line = line.prepare_line_for_printing(start=0, end=15)
    assert(sub_line.full_text() == 'one two three f')
    # The last segment should still have a standout attribute
    assert(sub_line[-1].curses_attrs == curses.A_BOLD)

    # Check width slicing with start and end
    sub_line = line.prepare_line_for_printing(start=4, end=8)
    assert(sub_line.full_text() == 'two ')
    sub_line = line.prepare_line_for_printing(start=1, end=11)
    assert(sub_line.full_text() == 'ne two thr')
    sub_line = line.prepare_line_for_printing(start=9, end=110)
    assert(sub_line.full_text() == 'hree four')
    assert(sub_line[-1].curses_attrs == curses.A_BOLD)
    try:
        line.prepare_line_for_printing(-10, 50)
    except Exception as exc:
        assert('Invalid start arg given' in str(exc))

    # Check highlighting for search string
    highlighted_line = line.highlight_str('two ', curses.A_STANDOUT)
    assert(len(highlighted_line) == len(line))
    assert(highlighted_line[1].text == 'two ')
    assert(highlighted_line[1].curses_attrs == curses.A_STANDOUT)
    highlighted_line = line.highlight_str('ee fo', curses.A_STANDOUT)
    assert(
        [seg.text for seg in highlighted_line.line_segments] ==
        ['one ', 'two ', 'thr', 'ee fo', 'ur'])
    assert(len(highlighted_line) == len(line)+1)
    assert(highlighted_line[-1].curses_attrs == curses.A_BOLD)
    assert(highlighted_line[-1].text == 'ur')
    assert(highlighted_line[-2].curses_attrs == curses.A_STANDOUT)
    assert(highlighted_line[-2].text == 'ee fo')
    highlighted_line = line.highlight_str('one two three ', curses.A_STANDOUT)
    assert(len(highlighted_line) == 2)
    assert(highlighted_line[0].text == 'one two three ')
    assert(highlighted_line[0].curses_attrs == curses.A_STANDOUT)
    assert(highlighted_line[1].text == 'four')
    line = content.Line(['testing one', 'testing two', 'testing ', 'three'])
    highlighted_line = line.highlight_str('testing', curses.A_STANDOUT)
    assert(len(highlighted_line) == 7)
    assert(
        [seg.text for seg in highlighted_line.line_segments] ==
        ['testing', ' one', 'testing', ' two', 'testing', ' ', 'three'])


def test_filepath_segment():
    """Test the Line object that has a segment tagged as a filepath"""
    line = content.Line(['one '])
    line.add_filepath_segment('/tmp/foo/bar  ', 'bar  ')
    line.add_segment('three')

    # Test standard operations
    sub_line = line.prepare_line_for_printing(start=0, end=25)
    assert(sub_line.full_text() == 'one /tmp/foo/bar  three')

    # Set path shortening to enabled and ensure we just see the path basename
    # with all original padding included
    opt_mngr = UserOptionManager()
    shorten_paths_opt = opt_mngr.get_option(SHORTEN_FILEPATHS_OPT)
    shorten_paths_opt.enable_option()
    sub_line = line.prepare_line_for_printing(start=0, end=25)
    assert(sub_line.full_text() == 'one bar  three')

    # Test a shortened path that is also clipped
    sub_line = line.prepare_line_for_printing(start=0, end=6)
    assert(sub_line.full_text() == 'one ba')

    # Test just the padding included from the shortened path
    sub_line = line.prepare_line_for_printing(start=7, end=35)
    assert(sub_line.full_text() == '  three')

    # Disable filepath shortening again and ensure we see the full path
    shorten_paths_opt.disable_option()
    sub_line = line.prepare_line_for_printing(start=0, end=25)
    assert(sub_line.full_text() == 'one /tmp/foo/bar  three')
