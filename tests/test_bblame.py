"""Tests for the full bblame application.
Stands up serveral instances of bblame using a curses test library called
hecate, presses keys, and validates the results."""
import hecate
import time
import os
import tempfile

# test files
TESTFILE_RENAMED = 'tests/testfiles/testfile-renamed'
TESTFILE = 'tests/testfiles/testfile2'

# XXX: Such a large width is used here because the hecate+tmux session was not
#      handling the content being longer than the width. Strange text wrapping
#      was happening.
WIDTH = 800
HEIGHT = 60

# SETUP

# Set timeouts
# If the environment is Bitbucket Pipelines, use much more liberal timeouts
make_env = ''
try:
    make_env = os.environ['MAKE_ENV']
except KeyError:
    print('No enviroment using default')

if make_env == 'pipelines':
    TIMEOUT_MS = 2000
    RETRY_MS = 100
    EXIT_TIMEOUT = 5
else:
    # The tests are run with debug enabled which slows down bblame
    # significantly, so the timeout for screen updates must be quite large.
    # Under normal, non-debugging use, bblame should respond in 200-300ms. But
    # this time also depends on system capabilities and to a larger extent the
    # file being blamed. If a large file with a long complex history is being
    # blamed, it will take longer for git to produce output for bblame to
    # display.
    TIMEOUT_MS = 500
    RETRY_MS = 50
    EXIT_TIMEOUT = 1


# Exceptions
class NoScreenContentError(Exception):
    """Exception to be raised when an attempt to read screenshot content from
    hecate fails with empty screenshosts"""
    pass


# UTILS

def get_screen_content(hrunner, timeout_ms=None, retry_ms=None):
    """Wait until some screen content shows up and return it.
    checking for content every <retry_ms> milliseconds.
    Wait for an initial time if for example a key has already been pressed (the
    key press will show up in the screen content fooling the simple algo below
    into thinking the app has printed to the screen).
    Timeout if we've waited too long for screen_content"""
    timeout_ms = timeout_ms if timeout_ms else TIMEOUT_MS
    retry_ms = retry_ms if retry_ms else RETRY_MS
    time.sleep(0.300)
    screen_content = hrunner.screenshot()
    num_loops = 0
    print('Finished initial wait for content')

    while not screen_content.strip():
        print('Content wasn\'t available, waiting %d ms' % retry_ms)
        if (num_loops * retry_ms) > timeout_ms:
            print('Timedout waiting for content!')
            raise NoScreenContentError()
        time.sleep(retry_ms/1000.0)
        screen_content = hrunner.screenshot()
        num_loops += 1

    if num_loops == 0:
        print('Didn\'t need to wait any longer for content')

    return screen_content


def _wait_for_new_screen(hrunner, old_screen):
    """Keeping waiting for a new screen that's different that the current/old
    screen passed in"""
    new_screen = get_screen_content(hrunner)
    retries = 3
    curr_retry = 0

    while new_screen == old_screen:
        if curr_retry > retries:
            raise Exception('Timed out waiting for new screen')
        print('Screen was not new, get a new screen')
        curr_retry += 1
        # No waiting necessary since get_screen_content will wait
        new_screen = get_screen_content(hrunner)

    print('We got a new screen!')
    return new_screen


def wait_for_new_screen(hrunner):
    """Grab a screen, then wait for a new screen"""
    print('Grab old screen for comparison')
    curr_screen = get_screen_content(hrunner)
    print('Now wait for new screen')
    new_screen = _wait_for_new_screen(hrunner, curr_screen)

    return new_screen


def press_key_and_wait_for_new_screen(hrunner, key):
    curr_screen = get_screen_content(hrunner)
    hrunner.press(key)
    if key == 'T':
        hrunner.press(key)
    new_screen = _wait_for_new_screen(hrunner, curr_screen)

    return new_screen


# TESTS

def test_help():
    """Open the help screen, try open it again, try run commands that shouldn't
    be allowed in help, return to normal mode
    and exit"""
    help_token = 'KEYS: ACTION - DESCRIPTION'
    help_from_help_token = 'Already displaying help!'
    with hecate.Runner('python', '-m', 'betterblame',  TESTFILE_RENAMED,
                       width=WIDTH, height=HEIGHT) as hrunner:
        assert(help_token in
               press_key_and_wait_for_new_screen(hrunner,
                                                 'h').splitlines()[0])
        assert(help_from_help_token in
               press_key_and_wait_for_new_screen(hrunner,
                                                 'h').splitlines()[-1])
        unsupported_actions = ['o', 'O', 'H', 'T', '<']
        for action in unsupported_actions:
            print('Running action: %s' % action)
            assert(help_token in
                   press_key_and_wait_for_new_screen(hrunner,
                                                     action).splitlines()[0])
        hrunner.press('Escape')
        hrunner.press('q')
        assert(hrunner.await_exit(timeout=EXIT_TIMEOUT) is None)


def test_drill_exit():
    """Drill into a line and, return to normal mode and exit"""
    with hecate.Runner('python', '-m', 'betterblame',  TESTFILE_RENAMED,
                       width=WIDTH, height=HEIGHT) as hrunner:
        hrunner.press('v')
        hrunner.press('Enter')
        hrunner.press('Escape')
        hrunner.press('q')
        assert(hrunner.await_exit(timeout=EXIT_TIMEOUT) is None)


def test_show():
    """Open the show screen for the file, return to normal, open show for
    specific line, show help, return to normal mode and exit
    """
    with hecate.Runner('python', '-m', 'betterblame', TESTFILE_RENAMED,
                       width=WIDTH, height=HEIGHT) as hrunner:
        hrunner.press('O')
        hrunner.press('Escape')
        hrunner.press('v')
        hrunner.press('o')
        hrunner.press('h')
        hrunner.press('Escape')
        hrunner.press('Escape')
        hrunner.press('q')
        assert(hrunner.await_exit(timeout=EXIT_TIMEOUT) is None)


def test_movement():
    """Test moving with the arrow keys. Specifically moving to the right, since
    this involves a complicated algorithm to ensure we use just the right
    amount of each segment of the line"""
    with hecate.Runner('python', '-m', 'betterblame', TESTFILE_RENAMED,
                       width=WIDTH, height=HEIGHT) as hrunner:
        # TODO: Add more movements soon
        hrunner.press('Right')
        hrunner.press('q')
        assert(hrunner.await_exit(timeout=EXIT_TIMEOUT) is None)


def test_search():
    """Open a file and search for a string"""
    with hecate.Runner('python', '-m', 'betterblame', TESTFILE_RENAMED,
                       width=WIDTH, height=HEIGHT) as hrunner:
        hrunner.press('/')
        hrunner.write('EDIT')
        hrunner.press('Enter')
        hrunner.press('Escape')
        hrunner.press('q')
        assert(hrunner.await_exit(timeout=EXIT_TIMEOUT) is None)


def test_snap_top_bottom():
    """Snap to the bottom then back to the top of the blame"""
    start_token = ('FIRST LINE Initial test string')
    end_token = ('Test string at the end of file')
    with hecate.Runner('python', '-m', 'betterblame', TESTFILE,
                       width=WIDTH, height=HEIGHT) as hrunner:
        assert(end_token in
               press_key_and_wait_for_new_screen(hrunner,
                                                 'G').splitlines()[-2])
        assert(start_token in
               press_key_and_wait_for_new_screen(hrunner,
                                                 'gg').splitlines()[0])
        hrunner.press('q')
        assert(hrunner.await_exit(timeout=EXIT_TIMEOUT) is None)


def test_jump_to_tail():
    """Test the jump to the first commit for the file"""
    first_commit_token = "This is the first commit"
    # Use the file that's been renamed and moved to ensure we get to the frist
    # commit, before the rename and move
    with hecate.Runner('python', '-m', 'betterblame', TESTFILE_RENAMED,
                       width=WIDTH, height=HEIGHT) as hrunner:
        assert(first_commit_token in
               press_key_and_wait_for_new_screen(hrunner, 'T').splitlines()[0])
        hrunner.press('q')
        assert(hrunner.await_exit(timeout=EXIT_TIMEOUT) is None)


def test_init_to_line_number():
    """Start bblame with a number to init the cursor to"""
    token_line = ('905c0fc7 tests/testfiles/testfile2 (Niko Oliveira '
                  '2016-11-12 11:42:58 -0800 83) Easily_searchable')
    with hecate.Runner('python', '-m', 'betterblame', TESTFILE, '+83',
                       '--revision', '905c0fc7',
                       width=WIDTH, height=HEIGHT) as hrunner:
        assert(token_line in _wait_for_new_screen(hrunner, ''))
        hrunner.press('q')
        assert(hrunner.await_exit(timeout=EXIT_TIMEOUT) is None)


def test_init_to_line_number_too_large():
    """Start bblame with a number to init the cursor to that is beyond the
    number of lines in the file"""
    token_line = ('d80f1d15 tests/testfiles/testfile2 (Niko Oliveira 2016-'
                  '11-11 21:51:03 -0800 91) Test string at the end of file')
    with hecate.Runner('python', '-m', 'betterblame', TESTFILE, '+888',
                       '--revision', 'd80f1d15',
                       width=WIDTH, height=HEIGHT) as hrunner:
        screen_content = _wait_for_new_screen(hrunner, '')
        # We want to assert that bblame snapped to the last line (-2, -1 is the
        # status bar)
        assert(token_line in screen_content.splitlines()[-2])
        hrunner.press('q')
        assert(hrunner.await_exit(timeout=EXIT_TIMEOUT) is None)


def test_init_to_line_number_too_small():
    """Start bblame with a number to init the cursor to that is negative.
    The cursor should just be init to the top of the file"""
    token_line = ('FIRST LINE Initial test string')
    with hecate.Runner('python', '-m', 'betterblame', TESTFILE, '+-88',
                       width=WIDTH, height=HEIGHT) as hrunner:
        screen_content = _wait_for_new_screen(hrunner, '')
        assert(token_line in screen_content.splitlines()[0])
        hrunner.press('q')
        assert(hrunner.await_exit(timeout=EXIT_TIMEOUT) is None)


def test_init_to_search_hit():
    """Start bblame with a search string and assert that the visual cursor is
    init to the first instance of the search string"""
    token_line = ('Easily_searchable_string_with_no_space')
    token_line_after_drill = 'Second test string the extends file line length'
    with hecate.Runner('python', '-m', 'betterblame', TESTFILE,
                       '+/Easily_searchable_string_with_no_spaces',
                       width=WIDTH, height=HEIGHT) as hrunner:
        screen_content = _wait_for_new_screen(hrunner, '')
        assert(token_line in screen_content.splitlines()[0])
        # drill past the line (we should be in visual mode already)
        screen_content = press_key_and_wait_for_new_screen(hrunner, 'd')
        # assert the easily searchable line is gone and that the expected line
        # exists
        assert(token_line not in screen_content.splitlines()[0])
        assert(token_line_after_drill in screen_content.splitlines()[0])
        # Return from drill and assert the easily searchable line is back
        screen_content = press_key_and_wait_for_new_screen(hrunner, 'f')
        screen_content = _wait_for_new_screen(hrunner, '')
        assert(token_line in screen_content.splitlines()[0])
        hrunner.press('q')
        assert(hrunner.await_exit(timeout=EXIT_TIMEOUT) is None)


def test_init_to_revision():
    """Start bblame at a particular revision"""
    token_line = ('d80f1d15 tests/testfiles/testfile2 (Niko Oliveira '
                  '2016-11-11 21:51:03')
    with hecate.Runner('python', '-m', 'betterblame', TESTFILE,
                       '--revision=d80f1d15',
                       width=WIDTH, height=HEIGHT) as hrunner:
        # Look for the commit info of a specific commit in that revision
        screen_content = _wait_for_new_screen(hrunner, '')
        with open('/tmp/tmp.out', 'w') as tmpout:
            tmpout.write(screen_content)
        assert(token_line in screen_content)
        hrunner.press('q')
        assert(hrunner.await_exit(timeout=EXIT_TIMEOUT) is None)


def test_utf8():
    """Open a revision under both versions of python that contains utf8 chars
    in the author's name, the line itself, and the commit message"""
    with hecate.Runner('python', '-m', 'betterblame', TESTFILE,
                       '--revision=70d979349',
                       width=WIDTH, height=HEIGHT) as hrunner:
        hrunner.press('q')
        assert(hrunner.await_exit(timeout=EXIT_TIMEOUT) is None)
    with hecate.Runner('python3', '-m', 'betterblame', TESTFILE,
                       '--revision=70d979349', '--debug',
                       width=WIDTH, height=HEIGHT) as hrunner:
        hrunner.press('q')
        assert(hrunner.await_exit(timeout=EXIT_TIMEOUT) is None)


# Failure Cases
def test_init_to_line_number_to_invalid_number():
    """Start bblame with a number to init the cursor to that is nonsensical,
    E.g. not a number"""
    try:
        with hecate.Runner('python', '-m', 'betterblame', TESTFILE, '+banana',
                           '--debug', width=WIDTH, height=HEIGHT) as hrunner:
            # TODO: find some way to check that we get the correct error
            #       message printed to the user.
            hrunner.await_exit(timeout=EXIT_TIMEOUT)
    except Exception as exc:
        assert(isinstance(exc, hecate.hecate.AbnormalExit))


def test_non_existant_path():
    """Start bblame with a path that doesn't exist"""
    try:
        with hecate.Runner('python', '-m', 'betterblame', 'foobarbananrama',
                           '--debug', width=WIDTH, height=HEIGHT) as hrunner:
            # TODO: find some way to check that we get the correct error
            #       message printed to the user.
            hrunner.await_exit(timeout=EXIT_TIMEOUT)
    except Exception as exc:
        assert(isinstance(exc, hecate.hecate.AbnormalExit))


def test_file_with_no_history():
    """Start bblame with a path that exist but no revisions committed.
    This should yield a nice error message, not a traceback"""
    try:
        cwd = os.getcwd()
        with tempfile.NamedTemporaryFile(dir=cwd) as tfile:
            with hecate.Runner('python', '-m', 'betterblame', tfile.name,
                               '--debug', width=WIDTH,
                               height=HEIGHT) as hrunner:
                # TODO: find some way to check that we get the correct error
                #       message printed to the user.
                hrunner.await_exit(timeout=EXIT_TIMEOUT)
    except Exception as exc:
        assert(isinstance(exc, hecate.hecate.AbnormalExit))
