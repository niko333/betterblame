"""Module that contains the current version of the project.
This module is read by the package itself and the setup.py config, so that
they are both always in sync."""
__version__ = '0.7.0'
