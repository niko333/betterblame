#!/usr/bin/env bash
# Install the packages necessary to develop and test bblame

# Must be root to run this script
if [ "$(id -u)" -ne 0 ]; then
    echo "This script must be run as root!"
    exit 1
fi

# apt packages
sudo apt-get install -y pylint make
# pip packages
sudo -H pip install flake8 twine

# Install the pre-commit hook
git_root=$(git rev-parse --show-toplevel)
cp "${git_root}"/scripts/pre-commit "${git_root}"/.git/hooks/
