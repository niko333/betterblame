#!/bin/bash

if [[ -z "$1" ]]; then
    echo "usage: $0 <path to lint>"
else
    lint_path="$1"
fi

rating_threshold=9

python3 -m pylint --fail-on=E,F --fail-under ${rating_threshold} -j0 "${lint_path}"/*.py

RC=$?
case $RC in
    0) echo "Success!" ;;
    *) echo "Either a Error/Fatal was detected or code rating is below threshold of ${rating_threshold}"; echo
esac

exit $RC
