#! /usr/bin/env bash
# Build a standalone executable binary with nuitka.

# Make this an argument in the future
BUILD_DIR='/tmp/bblame_build_temp'
PROJ_DIR=$(git rev-parse --show-toplevel)

if [ "$1" == "-r" ] && [ -n "$2" ]; then
    remotehost=$2
    echo "Copying files for remote build to ${BUILD_DIR}"
    # Note: it is intended that BUILD_DIR is expanded on the client side
    # shellcheck disable=SC2029
    ssh "${USER}"@"${remotehost}" "mkdir -p ${BUILD_DIR}"
    scp -r ${PROJ_DIR} "${USER}"@"${remotehost}":"${BUILD_DIR}"
    echo "Building on remote machine"
    # shellcheck disable=SC2029
    ssh "${USER}"@"${remotehost}" "cd ${BUILD_DIR}; pwd; nuitka --python-version=3.4 --standalone betterblame/bblame"
    echo "Cleaning up remote machine"
    ssh "${USER}"@"${remotehost}" rm -r "${BUILD_DIR}"/betterblame
    scp -r "${USER}"@"${remotehost}":"${BUILD_DIR}"/bblame.dist "${BUILD_DIR}"/
else
    mkdir -p ${BUILD_DIR}
    cd ${BUILD_DIR}
    echo "Starting build in ${BUILD_DIR}"
    nuitka --python-version=3.4 --standalone "$PROJ_DIR"/bblame
fi
echo "Done"
