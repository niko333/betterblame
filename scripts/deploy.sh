#! /usr/bin/env bash
# Deploy a nuitka built standalone binary to ~/.lib on a remote host.
# Link to the binary from ~/.bin (assuming that's on the users PATH).

# TODO: Make these arguments in the future
SOURCE_DIR='/tmp/bblame_build_temp'
if [ -z "$1" ] || [ -z "$2" ]; then
    echo "Please provide the hostname and user to deploy to and with"
    exit 1
else
    DEST_HOST=$1
    DEST_USER=$2
fi

LIB_DIR=".lib"
BIN_DIR=".bin"
HOSTING_DIR="public_html"

echo "Copying distribution dir to ${DEST_HOST}"
scp -r "${SOURCE_DIR}"/bblame.dist "${DEST_USER}"@"${DEST_HOST}":"${LIB_DIR}"
scp -r "${SOURCE_DIR}"/bblame.dist "${DEST_USER}"@"${DEST_HOST}":"${HOSTING_DIR}"
echo "Adding link to executable in sbin on ${DEST_HOST}"
# NOTE: it is desired that the expansions occur here on the client side
#       shellcheck disable=SC2087
ssh "${DEST_USER}"@"${DEST_HOST}" << EOF
set -x
mkdir -p "${BIN_DIR}"
ln -fs ~/"${LIB_DIR}"/bblame.dist/bblame.exe ~/"${BIN_DIR}"/bblame
EOF
